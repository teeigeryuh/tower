require 'sinatra'
require 'sinatra/multi_route' #sinatra contrib

load 'lib/status.rb'

get '/' do
	@ram = Stats.ram
	@uptime = Stats.uptime
	erb :status
end

get ['/killme', '/killme/'] do #kills it self, not so good
	Process.kill 9, Process.pid
end


#If a class in loaded on erb  113-118 rps; If loaded on front = 120-130
#Lesson? Don't load a class everytime a page is hit
