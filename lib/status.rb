class Status

	def initialize
		print "ok.... \n"
	end

	def ram (which = 0)
		ele = which
		ramget = `free -m`
		fetch = ramget.scan(/\d+/)
		usedram = fetch[1] #self explanatory
		maxram = fetch[0] #available

		if (ele == 0) #pretty ram usage
			return "#{usedram + '/' + maxram}"
		elsif (ele == 1)
			return "#{usedram}"
		else 
			return "#{maxram}"
		end
			
	end

	def uptime
		uptimeget = `uptime`
		fetch = uptimeget.match(/up(.+?),/).to_s
		fetch.gsub!("up", "")
		fetch.gsub!(',', '')
		return  "#{fetch}"
	end

end

Stats = Status.new
